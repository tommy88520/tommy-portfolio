import { IsString, IsDate, IsBoolean, IsArray } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateArticleDto {
  @IsString()
  @ApiProperty({ description: 'Image' })
  image: string;

  @IsString()
  @ApiProperty({ description: 'Title' })
  title: string;

  @IsDate()
  @ApiProperty({ default: new Date(), description: 'date' })
  date: Date;

  @IsString()
  @ApiProperty({ description: 'Content' })
  content: string;

  @IsBoolean()
  @ApiProperty({ default: false, description: 'Title' })
  isFeatured: boolean;

  @IsString()
  @ApiProperty({ default: 'en', description: 'lang' })
  lang: 'zhTw' | 'en';

  @IsArray()
  @ApiProperty({
    description: 'Tag',
    default: ['Taipei'],
    isArray: true,
  })
  tags: string[];
}
