import { PartialType } from '@nestjs/swagger';
import { CreateTaipeiTravelDto } from './create-taipei-travel.dto';

export class UpdateTaipeiTravelDto extends PartialType(CreateTaipeiTravelDto) {}
