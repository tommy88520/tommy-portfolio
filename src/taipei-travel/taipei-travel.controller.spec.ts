import { Test, TestingModule } from '@nestjs/testing';
import { TaipeiTravelController } from './taipei-travel.controller';
import { TaipeiTravelService } from './taipei-travel.service';

describe('TaipeiTravelController', () => {
  let controller: TaipeiTravelController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaipeiTravelController],
      providers: [TaipeiTravelService],
    }).compile();

    controller = module.get<TaipeiTravelController>(TaipeiTravelController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
