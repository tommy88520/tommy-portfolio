import { Module } from '@nestjs/common';
import { TaipeiTravelService } from './taipei-travel.service';
import { TaipeiTravelController } from './taipei-travel.controller';
import { Article } from './entities/article.entity';
import { Tag } from './entities/tag.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Article, Tag])],
  controllers: [TaipeiTravelController],
  providers: [TaipeiTravelService],
})
export class TaipeiTravelModule {}
