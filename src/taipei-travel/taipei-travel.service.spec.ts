import { Test, TestingModule } from '@nestjs/testing';
import { TaipeiTravelService } from './taipei-travel.service';

describe('TaipeiTravelService', () => {
  let service: TaipeiTravelService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaipeiTravelService],
    }).compile();

    service = module.get<TaipeiTravelService>(TaipeiTravelService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
