import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { CreateArticleDto } from './dto/createArticle.dto';
import { Article } from './entities/article.entity';

import { Tag } from './entities/tag.entity';

@Injectable()
export class TaipeiTravelService {
  constructor(
    @InjectRepository(Article)
    private readonly articleModel: Repository<Article>,
    @InjectRepository(Tag)
    private readonly tagModel: Repository<Tag>,
  ) {}
  async createArticle(createArticle: CreateArticleDto) {
    const { image, title, date, content, isFeatured, lang, tags } =
      createArticle;
    const article = this.articleModel.create({
      image,
      title,
      date,
      content,
      isFeatured,
      lang,
    });

    const newTags = [];
    for (const tag of tags) {
      const task = this.tagModel.create({ tag });
      await this.tagModel.save(task);
      newTags.push(task);
    }
    article.tags = newTags;

    return await this.articleModel.save(article);
  }
}
