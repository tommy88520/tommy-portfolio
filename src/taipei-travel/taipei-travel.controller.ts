import { Article } from './entities/article.entity';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TaipeiTravelService } from './taipei-travel.service';
import { CreateArticleDto } from './dto/createArticle.dto';

import { UpdateTaipeiTravelDto } from './dto/update-taipei-travel.dto';

@Controller('taipei-travel')
export class TaipeiTravelController {
  constructor(private readonly taipeiTravelService: TaipeiTravelService) {}

  @Post('create-article')
  create(@Body() createTaipeiTravelDto: CreateArticleDto) {
    return this.taipeiTravelService.createArticle(createTaipeiTravelDto);
  }
}
