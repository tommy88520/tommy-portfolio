import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Article } from './article.entity';

@Entity({ name: 'articleTag' })
export class Tag {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ default: '' })
  tag: string;

  @ManyToOne(() => Article, (item) => item.tags, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  public article: Article;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;
}
