import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { Tag } from './tag.entity';

@Entity()
export class Article {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column({ default: '' })
  image: string;

  @Column({ default: '' })
  title: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  date: Date;

  @Column({ default: '' })
  content: string;

  @Column({ default: false })
  isFeatured: boolean;

  @Column({ default: 'en' })
  lang: string;

  @OneToMany(() => Tag, (tag) => tag.article)
  @JoinColumn()
  public tags: Tag[];

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;
}
